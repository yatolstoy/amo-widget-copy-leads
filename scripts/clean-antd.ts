const fs = require('fs');

const path = `node_modules/ant-design-vue/es/style/core/index.less`;
const content = fs.readFileSync(path);
const replaced = content.toString().replace(/@import.+global.*/, '');
fs.writeFileSync(path, replaced);
