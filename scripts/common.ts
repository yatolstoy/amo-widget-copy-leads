import { Config } from 'amo-widget-builder'
import * as path from 'path'

export const config: Config = {
  name: {
    ru: 'Мой Первый Виджет'
  },
  description: {
    ru: 'Этот виджет невероятен'
  },
  shortDescription: {
    ru: 'Тут кратенько'
  },
  tourDescription: {
    ru: 'Установи меня!'
  },
  advancedSettingsTitle: {
    ru: 'Настройки тут'
  },
  version: '1.0.0',
  fakeConfig: {
    required: false
  },
  locales: {
    ru: {
      dp: {
        config: 'Настройки'
      }
    }
  },
  locations: ['settings', 'everywhere', 'digital_pipeline'],
  manifest: {
    dp: {
      webhook_url: `https://8c38-88-135-51-72.ngrok.io/copy/dp`,
      action_multiple: false,
      settings: {
        config: {
          name: 'dp.config',
          type: 'text',
          required: true
        }
      }
    }
  },
  bundleDir: path.resolve(__dirname, '../dist'),
  outDir: path.resolve(__dirname, '..')
}