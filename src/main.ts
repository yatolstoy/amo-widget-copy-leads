import { Widget } from './widget';

export default function (amoWidget: any) {
  let widget: Widget;
  const getWidget = async () => {
    if (process.env.NODE_ENV === 'production') {
      __webpack_public_path__ = `${amoWidget.params.path}/build/`;
    }
    if (!widget) {
      widget = new Widget(amoWidget);
    }
    return widget;
  };

  amoWidget.showLoading = () => {
    $(document).trigger({ type: 'page:overlay:show', is_list: 0 });
  };

  amoWidget.hideLoading = () => {
    $(document).trigger({ type: 'page:overlay:hide', is_list: 0 });
  };

  amoWidget.callbacks = {
    settings($box) {
      // if (amoWidget.params.status === 'not_configured') {
      //   amoWidget.background_install()
      // }
    },
    init: () => true,
    bind_actions: () => true,
    async render() {
      // (await getWidget()).onRender();
      return true;
    },
    destroy() {},
    leads: {
      async selected() {
        amoWidget.showLoading();
        const { selected } = amoWidget.list_selected();
        const leadIds: number[] = selected.map(({ id }) => id);
        await (await getWidget()).openModal(leadIds);
        return true;
      }
    },
    onSave: () => true
  };

  return amoWidget;
}
