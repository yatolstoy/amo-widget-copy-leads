import { App, AsyncComponentLoader } from 'vue';
import uniqid from 'uniqid';
// import { create } from 'amo-client-axios';
import { AxiosInstance } from 'axios';

export class Widget {
  constructor(private amoWidget: any) {}

  private modalApp: App;
  private modalId = uniqid(`modal-`);

  async openModal(leadIds: number[]) {
    if (this.modalApp) this.modalApp.unmount();

    let div = document.getElementById(this.modalId);
    if (!div) {
      div = document.createElement('div');
      div.id = this.modalId;
      document.getElementById('list_page_holder').appendChild(div);
    }

    if (!APP.isCard()) document.querySelectorAll('#widgets_block, #card_widgets_overlay').forEach(el => el.remove());

    this.modalApp = await this.createApp(() => import('./components/modal.vue'), { leadIds });
    this.modalApp.mount(div);
  }

  private async createApp(component: AsyncComponentLoader, props?: any) {
    const { createApp, defineAsyncComponent } = await import('vue');
    return createApp(defineAsyncComponent(component), props);
  }
}
