require('dotenv').config()

const { NODE_ENV = 'development', WEBPACK_PORT, API_URL, PUBLIC_PATH } = process.env
const webpack = require('webpack')
const publicPath = PUBLIC_PATH || `https://localhost:${WEBPACK_PORT}`

module.exports = {
  publicPath,
  configureWebpack: {
    output: {
      library: 'starterpackapp',
      libraryTarget: 'umd'
    },
    plugins: [new webpack.EnvironmentPlugin({ NODE_ENV, API_URL })]
  },
  chainWebpack: config => {
    config.plugins.delete('html')
    config.plugins.delete('preload')
    config.plugins.delete('prefetch')
    config.optimization.delete('splitChunks')
  },
  css: {
    extract: false,
    loaderOptions: {
      less: {
        lessOptions: {
          javascriptEnabled: true
        }
      }
    }
  },
  devServer: {
    https: true,
    host: 'localhost',
    port: WEBPACK_PORT,
    allowedHosts: "all",
    headers: {
      'Access-Control-Allow-Origin': '*',
      'Access-Control-Allow-Headers': '*',
      'Access-Control-Allow-Methods': '*'
    }
  }
}
